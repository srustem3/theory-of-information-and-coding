from queue import PriorityQueue


class Node:
    def __init__(self, frequency=0, letter='', left=None, right=None):
        self.frequency = frequency
        self.letter = letter
        self.left = left
        self.right = right


queue = PriorityQueue()
node_lst = [Node(frequency=9, letter='a'),
            Node(frequency=2, letter='b'),
            Node(frequency=5, letter='c')]
size = 0

for node in node_lst:
    size += 1
    queue.put((node.frequency, node))

while size > 1:
    node1 = queue.get()[1]
    node2 = queue.get()[1]
    parent = Node(frequency=node1.frequency + node2.frequency, left=node1, right=node2)
    queue.put((parent.frequency, parent))
    size -= 1

root = queue.get()[1]


def visit(node, before_code):
    if node.left is None or node.right is None:
        print('Letter {}, code {}'.format(node.letter, before_code))
        return

    visit(node.left, before_code + '0')
    visit(node.right, before_code + '1')


visit(root, '')
