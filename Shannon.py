import math


class Node:
    def __init__(self, probability=0.0, letter=''):
        self.probability = probability
        self.letter = letter


def float_to_bin_fixed(f, size):
    n, d = f.as_integer_ratio()  # f = numerator / denominator
    formatted = f'{n:0{d.bit_length()-1}b}'
    return f'{formatted:0<{size}}'


node_lst = [Node(probability=0.1, letter='a'),
            Node(probability=0.2, letter='b'),
            Node(probability=0.1, letter='c'),
            Node(probability=0.1, letter='d'),
            Node(probability=0.35, letter='e'),
            Node(probability=0.15, letter='f')]
node_lst.sort(key=lambda n: n.probability, reverse=True)

before_sum = 0.0
for i in node_lst:
    length = math.ceil(-math.log2(i.probability))
    print('Letter {}, code {}'.format(i.letter, float_to_bin_fixed(before_sum, length)[:length]))
    before_sum += i.probability
