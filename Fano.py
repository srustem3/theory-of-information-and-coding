class Node:
    def __init__(self, frequency=0, letter=''):
        self.frequency = frequency
        self.letter = letter


node_lst = [Node(frequency=9, letter='a'),
            Node(frequency=2, letter='b'),
            Node(frequency=5, letter='c')]
node_lst.sort(key=lambda n: n.frequency, reverse=True)


def divide(sorted_lst, before_code):
    if len(sorted_lst) == 1:
        print('Letter {}, code {}'.format(sorted_lst[0].letter, before_code))
        return
    total_sum = 0
    for i in sorted_lst:
        total_sum += i.frequency
    mean = total_sum / 2
    difference = mean
    local_sum = 0
    center_position = 0
    for i, el in enumerate(sorted_lst):
        local_sum += el.frequency
        if abs(mean - local_sum) >= abs(difference):
            center_position = i
            break
        difference = mean - local_sum
    divide(sorted_lst[:center_position], before_code + '0')
    divide(sorted_lst[center_position:], before_code + '1')


divide(node_lst, '')
